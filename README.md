# Aframe Light Probe

A LightProbe Component

## Usage

```html
<a-entity lightProbe="cubeUrls: {url1}, {url1}, ..."></a-entity>
```

If using with angular, import it in the `polyfills.ts`
