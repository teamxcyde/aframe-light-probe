if (typeof AFRAME === 'undefined') {
  throw new Error('Component attempted to register before AFRAME was available.');
}

AFRAME.registerComponent('light-probe', {
  schema: {
    cubeUrls: { type: 'array', default: [] },
  },

  init: function() {
    this.lightProbe = null;
    this.cubeTextureLoader = new THREE.CubeTextureLoader();
  },

  update: function() {
    var data = this.data;

    this.lightProbe = new THREE.LightProbe(undefined, 1);
    this.removeLight();
    this.loadLightProbe(data.cubeUrls);
  },

  remove: function() {
    if (!this.lightProbe) {
      return;
    }
    this.removeLight();
  },

  removeLight: function() {
    this.el.removeObject3D('light');
  },

  loadLightProbe: function(cubeUrls) {
    var self = this;
    var el = this.el;
    var cubeTextureLoader = this.cubeTextureLoader;

    cubeTextureLoader.load(cubeUrls, function(cubeTexture) {
      cubeTexture.encoding = THREE.sRGBEncoding;
      self.lightProbe.copy(THREE.LightProbeGenerator.fromCubeTexture(cubeTexture));
      el.setObject3D('light', self.lightProbe);
    });
  },
});
